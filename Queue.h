#pragma once
#include "SimObj.h"
#include "Entity.h"

class Queue : public SimObj {
public:
	Queue();

	void ScheduleArrivalIn(Time deltaTime, Entity* en);


private:
	class ArriveEvent;

	void Arrive(Entity* en);
};
