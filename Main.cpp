#include <iostream>
#include "Queue.h"
#include "SimObj.h"

using namespace std;

int main() {

	Queue queue;	//creat our Queue object based on Queue class
	queue.ScheduleArrivalIn(0, new Entity());
	queue.ScheduleArrivalIn(1.0, new Entity());
	queue.ScheduleArrivalIn(2.0, new Entity());
	queue.ScheduleArrivalIn(0.5, new Entity());
	queue.ScheduleArrivalIn(0.7, new Entity());
	queue.ScheduleArrivalIn(2.5, new Entity());
	queue.ScheduleArrivalIn(2.0, new Entity());
	queue.PrintEvents();

	SimObj::RunSimulation();

	system("pause");
}