#include "Queue.h"
#include "SimObj.h"
#include <iostream>

using namespace std;

Queue::Queue() {

}

class Queue::ArriveEvent : public Event {
public:
	ArriveEvent(Queue* queue, Entity* en) {
		_queue = queue;
		_en = en;
	}

	void Execute() {
		_queue->Arrive(_en);
	}

private:
	Queue* _queue;
	Entity* _en;
};

void Queue::Arrive(Entity* en) {
	cout << "The entity has arrived. " << en->GetID() << endl;
}

void Queue::ScheduleArrivalIn(Time deltaTime, Entity* en) {
	ScheduleEventIn(deltaTime, new ArriveEvent(this, en));
}
