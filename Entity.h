#pragma once

typedef double Time;

class Entity {
private:
	int _id;			//holds the unique entity id
	static int _nextID;	//holds the next id in sequence

public:
	Entity();

	int GetID();	//returns the id of a particular entity
};
