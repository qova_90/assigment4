#pragma once

#include "Event.h"

typedef double Time;

class SimObj {
public:
	static Time GetCurrentSimTime();	//return current simulation time

	static void RunSimulation();		//called to start simulation with no end time

	static void RunSimulation(Time endTime);	//starts sim with specific end time

	static void PrintEvents(); // Print out all events in eventSet

protected:
	SimObj();

	void ScheduleEventIn(Time deltaTime, Event* ev);

	void ScheduleEventAt(Time time, Event* ev);

private:
	static  Time  _currentTime;

	class EventSet;

	static  EventSet  _eventSet;

};
