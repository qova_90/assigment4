#include "SimObj.h"
#include <iostream>
#include <iomanip>

class SimObj::EventSet {
public:
	EventSet() {
		_first = nullptr;
	}

	//adds an event to our event set
	void AddEvent(Time time, Event* ev) {
		//Create a new eventnode
		//Check to see if this is the first event, if true add to head of event set
		//If not first node, is this event before the next event. If true insert at head of list
		//If not true then add to the appropriate place in the event set

		EventNode* newEventNode = new EventNode(time, ev);
		/*
			Iterative function to insert new node
		*/
		//AddEventIterator(_first, newEventNode);
		if (_first == nullptr) {
			_first = newEventNode;
			return;
		}
		else if (newEventNode->_time < _first->_time) {
			newEventNode->_next = _first;
			_first = newEventNode;
			return;
		}
		else {
			AddEventIterator(_first, newEventNode);
		}
		
	}

	//retrieves the next event in line
	Event* GetEvent() {
		//Get the pointer to the first event in the event set
		//Update the Event Set pointer to event immediately after one just retrieved.
		EventNode* _temp = _first;
		_first = _temp->_next;
		return _temp->_ev;
	}

	void ListEvent() {
		//We have to retrieve each event in the event set and list it out
		 
		EventNode* _eventNode = _first;
		int count = 0;

		while (_eventNode!= nullptr) {
			std::cout <<"Event"<< std::setw(5)<< "Scheduled at Time"<< std::setw(5) << _eventNode->_time <<std::endl;
			_eventNode = _eventNode->_next;
		}
	}

	//get current simulation time
	Time GetTime() {
		//return the event time of the next event in the event set
		return Time();
	}

	//tells if any events remain in the event set
	bool HasEvent() {
		//return true if event set has more nodes in its structure
		return _first != nullptr;
	}

private:
	struct EventNode
	{
		EventNode(Time time, Event* ev) {
			_time = time;
			_ev = ev;
			_next = nullptr;
		}

		Time _time;
		Event* _ev;
		EventNode* _next;

	};

	EventNode* _first;

	// Iterative function to add An eventNode at appropriate time range
	void AddEventIterator(EventNode* currentNode, EventNode* newNode) {
		if (currentNode->_next == nullptr) {
			currentNode->_next = newNode;
			return;
		}
		else {
			if (newNode->_time < currentNode->_next->_time) {
				newNode->_next = currentNode->_next;
				currentNode->_next = newNode;
				return;
			}

			else {
				AddEventIterator(currentNode->_next, newNode);
			}
		}
	}
};


//Instantiate the static members of SimObject class else linker errors will be generated
//http://www.cplusplus.com/forum/general/227613/
Time SimObj::_currentTime;
SimObj::EventSet SimObj::_eventSet;

SimObj::SimObj()
{

}

void SimObj::ScheduleEventIn(Time deltaTime, Event* ev)
{
	//adds a event node at the appropriate time in the event set
	_eventSet.AddEvent(_currentTime + deltaTime, ev);
}

void SimObj::ScheduleEventAt(Time time, Event* ev)
{
	//adds an event node at the appropriate absolute time in the event set.
	_eventSet.AddEvent(time, ev);
}

Time SimObj::GetCurrentSimTime()
{
	return _currentTime;
}

void SimObj::RunSimulation() {
	//are there more events in the event set, if not stop
	//set sim time to the next event time
	//retrieve the next event
	//allow that event to execute
	while (_eventSet.HasEvent()) {
		Event * runEvent = _eventSet.GetEvent();
		_currentTime = _eventSet.GetTime();
		runEvent->Execute();
		delete runEvent;
	}
}

void SimObj::RunSimulation(Time endTime)
{
	//are there more events in the event set and are those events before the end time?
	//if not stop, otherwise set sim time to the next event time
	//retrieve the next event
	//all that event to execute

	while (_eventSet.HasEvent() && _eventSet.GetTime() < endTime) {
		Event* runEvent = _eventSet.GetEvent();
		_currentTime = _eventSet.GetTime();
		runEvent->Execute();
		delete runEvent;
	}
}

void SimObj::PrintEvents() {
	// Call list event in event set to print all events
	_eventSet.ListEvent();
}